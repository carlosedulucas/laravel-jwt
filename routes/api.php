<?php

// use Illuminate\Http\Request;

// /*
// |--------------------------------------------------------------------------
// | API Routes
// |--------------------------------------------------------------------------
// |
// | Here is where you can register API routes for your application. These
// | routes are loaded by the RouteServiceProvider within a group which
// | is assigned the "api" middleware group. Enjoy building your API!
// |
// */

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


// $this->get('categories', 'Api\CategoryController@index');
// $this->post('categories', 'Api\CategoryController@store');
// $this->put('categories/{id}', 'Api\CategoryController@update');
// $this->delete('categories/{id}', 'Api\CategoryController@destroy');

// rota de listas 
$this->group([
    'prefix' => 'v1', 
    'namespace' => 'Api\v1',
], function() {
    $this->get('profissaoList', 'ProfissaoController@index');
    $this->get('religiaoList', 'ReligiaoController@index');
    $this->get('racacorList', 'RacacorController@index');
    $this->get('estadocivilList', 'EstadocivilController@index');
});

$this->post('auth', 'Auth\AuthApiController@authenticate');
$this->post('register', 'Auth\RegisterController@create');
$this->get('me', 'Auth\AuthApiController@getAuthenticatedUser');
$this->post('auth-refresh', 'Auth\AuthApiController@refreshToken');

// rotas autenticadas
$this->group([
    'prefix' => 'v1', 
    'namespace' => 'Api\v1',
    'middleware' => 'auth:api'

], function() {
    $this->get('categories/{id}/products', 'CategoryController@products');
    $this->apiResource('categories', 'CategoryController');
    $this->apiResource('products', 'ProductController');
    $this->apiResource('pessoas', 'PessoaController');
    $this->apiResource('racacors', 'RacacorController');
    $this->apiResource('religiaos', 'ReligiaoController');
    $this->apiResource('profissaos', 'ProfissaoController');
});