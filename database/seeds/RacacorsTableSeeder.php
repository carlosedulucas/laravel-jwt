<?php

use Illuminate\Database\Seeder;
use App\Models\Racacor;

class RacacorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // fonte IBGE
        Racacor::create([
            'racacor' => 'Branca',
        ]);    
        Racacor::create([
            'racacor' => 'Parda',
        ]);        
        Racacor::create([
            'racacor' => 'Preta',
        ]);    
        Racacor::create([
            'racacor' => 'Amarela',
        ]);    
        Racacor::create([
            'racacor' => 'Indígena',
        ]);    
        Racacor::create([
            'racacor' => 'Sem Declaração',
        ]);
    }
}
