<?php

use Illuminate\Database\Seeder;
use App\Models\Profissao;


class ProfissaosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Profissao::create([
            'cbo' =>'6125-10' ,
            'profissao' => 'Abacaxicultor Sinônimo',
        ]);
        
        Profissao::create([
        'cbo' =>'2631-05', 
        'profissao' => 'Abade Sinônimo',
        ]);
        Profissao::create([
        'cbo' =>'2631-05' ,
        'profissao' => 'Abadessa S,inônimo',
        ]);
        Profissao::create(['cbo' =>'6220-20' ,
        'profissao' => 'Abanador na agricultura Sinônimo',
        ]);
        Profissao::create(['cbo' =>'8621-20' ,
        'profissao' => 'Abastecedor de caldeira Sinônimo',
        ]);
        Profissao::create(['cbo' =>'8621-60' ,
        'profissao' => 'Abastecedor de combustível de aeronave Sinônimo',
        ]);
        Profissao::create(['cbo' =>'7842-05' ,
        'profissao' => 'Abastecedor de linha de produção Sinônimo',
        ]);
        Profissao::create(['cbo' =>'7842-05' ,
        'profissao' => 'Abastecedor de máquinas de linha de produção Sinônimo',
        ]);
        Profissao::create(['cbo' =>'6326-05' ,
        'profissao' => 'Abastecedor de silos de carvão Sinônimo',
        ]);
        Profissao::create(['cbo' =>'8485-05' ,
        'profissao' => 'Abatedor Ocupação',
        ]);
        Profissao::create(['cbo' =>'8485-05' ,
        'profissao' => 'Abatedor em matadouro Sinônimo',
        ]);
        Profissao::create(['cbo' =>'6321-25' ,
        'profissao' => 'Abatedor - na extração de madeira Sinônimo',
        ]);
        Profissao::create(['cbo' =>'8485-05' ,
        'profissao' => 'Abatedor de animais Sinônimo',
        ]);
        Profissao::create(['cbo' =>'8485-05' ,
        'profissao' => 'Abatedor de aves Sinônimo',
        ]);
        Profissao::create(['cbo' =>'8485-05' ,
        'profissao' => 'Abatedor de gado Sinônimo',
        ]);
        Profissao::create(['cbo' =>'8485-05',
        'profissao' => 'Abatedor de galinha Sinônimo',
        ]);
        Profissao::create(['cbo' =>'8485-05' ,
        'profissao' => 'Abatedor de porco Sinônimo',
        ]);
        Profissao::create(['cbo' =>'6134-05' ,
        'profissao' => 'Abelheiro Sinônimo',
        ]);
        Profissao::create(['cbo' =>'7114-10' ,
        'profissao' => 'Abridor - nas salinas Sinônimo',
        ]);
        Profissao::create(['cbo' =>'7612-05' ,
        'profissao' => 'Abridor de lã Sinônimo',]);
          
    }
}

