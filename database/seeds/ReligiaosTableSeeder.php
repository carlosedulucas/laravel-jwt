<?php

use Illuminate\Database\Seeder;
use App\Models\Religiao;


class ReligiaosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Fonte IBGE
        
        Religiao::create([
            'religiao' => 'Católica Romana'
        ]);

        Religiao::create([
            'religiao' => 'Católica Brasileira'
        ]);

        Religiao::create([
            'religiao' => 'Católica Ortodoxa'
        ]);

        Religiao::create([
            'religiao' => 'Adventista'
        ]);

        Religiao::create([
            'religiao' => 'Batista'
        ]);

        Religiao::create([
            'religiao' => 'Luterana'
        ]);
        
        Religiao::create([
            'religiao' => 'Metodista'
        ]);

        Religiao::create([
            'religiao' => 'Presbiteriana'
        ]);

        Religiao::create([
            'religiao' => 'Outra evangélica tradicional'
        ]);

        Religiao::create([
            'religiao' => 'Evangélica tradicional não determinada'
        ]);

        Religiao::create([
            'religiao' => 'Assembléia de Deus'
        ]);
        
        Religiao::create([
            'religiao' => 'Congregação Cristã do Brasil'
        ]);

        Religiao::create([
            'religiao' => 'Deus é Amor'
        ]);

        Religiao::create([
            'religiao' => 'Evangelho Quadrangular'
        ]);

        Religiao::create([
            'religiao' => 'Tradicional Renovada'
        ]);

        Religiao::create([
            'religiao' => 'Universal do Reino de Deus'
        ]);

        Religiao::create([
            'religiao' => 'Outra evangélica pentecostal'
        ]);

        Religiao::create([
            'religiao' => 'Evangélica pentecostal não determinada'
        ]);

        Religiao::create([
            'religiao' => 'Cristã reformada não determinada'
        ]);

        Religiao::create([
            'religiao' => 'Mórmon'
        ]);

        Religiao::create([
            'religiao' => 'Testemunhas de Jeová'
        ]);

        Religiao::create([
            'religiao' => 'Outra Neo-cristã'
        ]);

        Religiao::create([
            'religiao' => 'Espírita Umbanda'
        ]);

        Religiao::create([
            'religiao' => 'Espírita Candomblé'
        ]);

        Religiao::create([
            'religiao' => 'Judaica ou Israelita'
        ]);

        Religiao::create([
            'religiao' => 'Budista'
        ]);

        Religiao::create([
            'religiao' => 'Messiânica'
        ]);

        Religiao::create([
            'religiao' => 'Outra oriental'
        ]);

        Religiao::create([
            'religiao' => 'Outras'
        ]);

        Religiao::create([
            'religiao' => 'Sem religião'
        ]);

        Religiao::create([
            'religiao' => 'Sem declaração'
        ]);
    }
}




