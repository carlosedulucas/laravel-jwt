<?php

use Illuminate\Database\Seeder;
use App\Models\Estadocivil;

class EstadocivilsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Estadocivil::create([
            'estadoCivil' => 'Casado'
        ]);
        Estadocivil::create([
            'estadoCivil' => 'Víuvo'
        ]);
        Estadocivil::create([
            'estadoCivil' => 'Divorsiado'
        ]);
        Estadocivil::create([
            'estadoCivil' => 'Solteiro'
        ]);
        Estadocivil::create([
            'estadoCivil' => 'Separado'
        ]);
        Estadocivil::create([
            'estadoCivil' => 'União Estável'
        ]);
        Estadocivil::create([
            'estadoCivil' => 'Outro'
        ]);
    }
}
