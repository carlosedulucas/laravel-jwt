<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->date('dataNasc')->nullable();
            $table->char('cpf',11)->unique()->nullable();
            $table->string('naturalidade',45)->nullable();
            $table->string('procedencia', 45)->nullable();
            $table->char('cep',8)->nullable();
            $table->string('numero', 20)->nullable();
            $table->enum('genero', ['M', 'F', 'I'] )->nullable();
            $table->string('complemento', 20)->nullable();
            
            $table->integer('religiao_id')->unsigned()->nullable();
            $table->integer('racacor_id')->unsigned()->nullable();
            $table->integer('profissao_id')->unsigned()->nullable();
            $table->integer('estadocivil_id')->unsigned()->nullable();

            $table->foreign('religiao_id')->references('id')->on('religiaos')->onDelete('cascade');
            $table->foreign('racacor_id')->references('id')->on('racacors')->onDelete('cascade');
            $table->foreign('profissao_id')->references('id')->on('profissaos')->onDelete('cascade');
            $table->foreign('estadocivil_id')->references('id')->on('estadocivils')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
