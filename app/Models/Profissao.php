<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profissao extends Model
{
    protected $fillable = ['cbo','profissao'];

    public function getResults($profissao = null)
    {
        
        if (!$profissao){
            return $this->get();
        }
        return $this->where('profissao', 'LIKE', "%{$profissao}%")    
        ->get();
      
    } 
}
