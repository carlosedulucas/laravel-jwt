<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Estadocivil extends Model
{
    protected $fillable = ['estadoCivil'];

    public function getResults($estadocivil = null)
    {
        
        if (!$estadocivil){
            return $this->get();
        }
        return $this->where('estadoCivil', 'LIKE', "%{$estadocivil}%")    
        ->get();
      
    } 
}
