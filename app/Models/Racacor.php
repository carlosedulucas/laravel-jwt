<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Racacor extends Model
{
    protected $fillable = ['racacor'];

    public function getResults($racacor = null)
    {
        
        if (!$racacor){
            return $this->get();
        }
        return $this->where('racacor', 'LIKE', "%{$racacor}%")    
        ->get();
      
    } 
}
