<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Religiao extends Model
{
    protected $fillable = ['religiao'];

    public function getResults($religiao = null)
    {
        
        if (!$religiao){
            return $this->get();
        }
        return $this->where('religiao', 'LIKE', "%{$religiao}%")    
        ->get();
      
    } 
}
