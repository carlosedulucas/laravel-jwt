<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Racacor;

class RacacorController extends Controller
{
    private $racacor, $totalPage = 20;

    public function __construct(Racacor $racacor)
    {
        $this->racacor = $racacor;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $racacor = $this->racacor->getResults($request->all(),$this->totalPage);
        return response()->json($racacor);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ( !$racacor = $this->racacor->create($request->all()))
        return response()->json(['error' => 'error_insert'], 500);

        return response()->json($racacor,201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\racacor  $racacor
     * @return \Illuminate\Http\Response
     */
    public function show(racacor $racacor)
    {
        if (!$racacor = $this->racacor->find($racacor->id))
        return response()->json(['error' => 'Não encontrado'],404);

        return response()->json($racacor);   
     }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\racacor  $racacor
     * @return \Illuminate\Http\Response
     */
    public function edit(racacor $racacor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\racacor  $racacor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, racacor $racacor)
    {
        if (!$racacor = $this->racacor->find($racacor->id))
        return response()->json(['error' => 'Não encontrado'],404);

        $racacor->update($request->all());

        return response()->json($racacor,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\racacor  $racacor
     * @return \Illuminate\Http\Response
     */
    public function destroy(racacor $racacor)
    {
        if (!$racacor = $this->racacor->find($racacor->id))
        return response()->json(['error' => 'Não encontrado'],404);

        $racacor->delete();

        return response()->json(['sucess'=> true],204);
    }
}
