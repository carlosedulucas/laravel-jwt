<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\Estadocivil;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EstadocivilController extends Controller
{
    private $estadocivil, $totalPage = 20;

    public function __construct(Estadocivil $estadocivil)
    {
        $this->estadocivil = $estadocivil;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $estadocivil = $this->estadocivil->getResults($request->all(),$this->totalPage);
        return response()->json($estadocivil);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ( !$estadocivil = $this->estadocivil->create($request->all()))
        return response()->json(['error' => 'error_insert'], 500);

        return response()->json($estadocivil,201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Estadocivil  $estadocivil
     * @return \Illuminate\Http\Response
     */
    public function show(Estadocivil $estadocivil)
    {
        if (!$estadocivil = $this->estadocivil->find($estadocivil->id))
        return response()->json(['error' => 'Não encontrado'],404);

        return response()->json($estadocivil);  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Estadocivil  $estadocivil
     * @return \Illuminate\Http\Response
     */
    public function edit(Estadocivil $estadocivil)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Estadocivil  $estadocivil
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Estadocivil $estadocivil)
    {
        if (!$estadocivil = $this->estadocivil->find($estadocivil->id))
        return response()->json(['error' => 'Não encontrado'],404);

        $estadocivil->update($request->all());

        return response()->json($estadocivil,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Estadocivil  $estadocivil
     * @return \Illuminate\Http\Response
     */
    public function destroy(Estadocivil $estadocivil)
    {
        if (!$estadocivil = $this->estadocivil->find($estadocivil->id))
        return response()->json(['error' => 'Não encontrado'],404);

        $estadocivil->delete();

        return response()->json(['sucess'=> true],204);
    }
}
