<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Religiao;

class ReligiaoController extends Controller
{
    private $religiao, $totalPage = 20;

    public function __construct(Religiao $religiao)
    {
        $this->religiao = $religiao;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $religiao = $this->religiao->getResults($request->all(),$this->totalPage);
        return response()->json($religiao);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ( !$religiao = $this->religiao->create($request->all()))
        return response()->json(['error' => 'error_insert'], 500);

        return response()->json($religiao,201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Religiao  $religiao
     * @return \Illuminate\Http\Response
     */
    public function show(Religiao $religiao)
    {
        if (!$religiao = $this->religiao->find($religiao->id))
        return response()->json(['error' => 'Não encontrado'],404);

        return response()->json($religiao);   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Religiao  $religiao
     * @return \Illuminate\Http\Response
     */
    public function edit(Religiao $religiao)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Religiao  $religiao
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Religiao $religiao)
    {
        if (!$religiao = $this->religiao->find($religiao->id))
        return response()->json(['error' => 'Não encontrado'],404);

        $religiao->update($request->all());

        return response()->json($religiao,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Religiao  $religiao
     * @return \Illuminate\Http\Response
     */
    public function destroy(Religiao $religiao)
    {
        if (!$religiao = $this->religiao->find($religiao->id))
        return response()->json(['error' => 'Não encontrado'],404);

        $religiao->delete();

        return response()->json(['sucess'=> true],204);
    }
}
