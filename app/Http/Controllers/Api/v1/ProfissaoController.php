<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\Profissao;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class ProfissaoController extends Controller
{
    private $profissao, $totalPage = 20;

    public function __construct(Profissao $profissao)
    {
        $this->profissao = $profissao;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $profissao = $this->profissao->getResults($request->all(),$this->totalPage);
        return response()->json($profissao);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ( !$profissao = $this->profissao->create($request->all()))
        return response()->json(['error' => 'error_insert'], 500);

        return response()->json($profissao,201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Profissao  $profissao
     * @return \Illuminate\Http\Response
     */
    public function show(Profissao $profissao)
    {
        if (!$profissao = $this->profissao->find($profissao->id))
        return response()->json(['error' => 'Não encontrado'],404);

        return response()->json($profissao);   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Profissao  $profissao
     * @return \Illuminate\Http\Response
     */
    public function edit(Profissao $profissao)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Profissao  $profissao
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profissao $profissao)
    {
        if (!$profissao = $this->profissao->find($profissao->id))
        return response()->json(['error' => 'Não encontrado'],404);

        $profissao->update($request->all());

        return response()->json($profissao,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Profissao  $profissao
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profissao $profissao)
    {
        if (!$profissao = $this->profissao->find($profissao->id))
        return response()->json(['error' => 'Não encontrado'],404);

        $profissao->delete();

        return response()->json(['sucess'=> true],204);
    }
}
